In order to run this ruby script yourself, Ruby should be installed, and
bundler should be installed. While in this project run:

'''
$ bundler install
$ ruby lib/filter.rb
'''

This particular filter assumes we are talking about the legislators.csv file.
If no parameters are given when calling the script, it should create two new
csv files in the 'rec' folder. One called: 'youngdem.csv' for the Democrats
under 45, and one called 'socialrep.csv' for the republicans with Twitter and
YouTube

This script could also take a parameter for the path to the legislator.csv file
and the second parameter for which directory to save the two csv files in. They
would still be named the same.

