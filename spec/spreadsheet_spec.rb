require 'model/spreadsheet'
require 'model/condition'

file = "rec/legislators.csv"
s = Spreadsheet.new(file)

RSpec.describe Spreadsheet, "#headers" do
  context "After initializing a spreadsheet" do
    it "returns an array of headers" do
      expect(s.headers.length).to eq 29
      expect(s.headers.first).to eq "title"
    end
  end
end
RSpec.describe Spreadsheet, "#line_check" do
  context "For some arbitrary line of a file" do
    l = "" 
    File.open(file) { |f|
      f.readline
      l = f.readline
    } 
    it "can check if a condition is true" do
      c1 = Condition.new("party") { |check|
        check == "D"
      }
      c2 = Condition.new("party") { |check|
        check == "R"
      }
      expect(s.line_check(l, [c1])).to eq true
      expect(s.line_check(l, [c2])).to eq false
    end
    it "can check multiple conditions" do
      c1 = Condition.new("party") { |check|
        check == "D"
      }
      c2 = Condition.new("gender") { |check|
        check == "M"
      }
      c3 = Condition.new("party") { |check|
        check == "R"
      }
      expect(s.line_check(l, [c1, c2])).to eq true
      expect(s.line_check(l, [c2, c3])).to eq false
    end
    it "returns false with a bad condition header" do
      c1 = Condition.new("bad") { |check|
        true
      }
      expect(s.line_check(l, [c1])).to eq false
    end
  end
end
RSpec.describe Spreadsheet, "#filter" do
  new_file = "rec/filter.csv"
  context "after filtering" do
    c1 = Condition.new("party") { |check|
      check == "D"
    }
    s.filter(new_file, c1)
    it "creates/rewrites file" do
      expect(File.exist?(new_file)).to eq true
    end
    it "has fewer rows than original" do
      orig_rows = 0
      new_rows = 0
      File.open(file) { |f|
        orig_rows = f.readlines.length
      }
      File.open(new_file) { |f|
        new_rows = f.readlines.length
      }
      expect(new_rows < orig_rows).to eq true
    end
  end
end
