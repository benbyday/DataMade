require 'model/condition'

RSpec.describe Condition, "#test" do
  context "after initialization" do
    c = Condition.new("") { |check|
      check == 1
    }
    it "condition holds" do
      expect(c.test(1)).to eq(true)
      expect(c.test(0)).to eq(false)
    end
  end
end
