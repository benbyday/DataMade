require 'csv'

class Spreadsheet
  def initialize(file)
    @file = file
    File.open(@file) { |file|
      @headers = file.readline.strip.split(',')
    }
  end
  
  def headers
    Array.new(@headers)
  end

  def line_check(line, cond_arr)
    cond_arr.all? { |cond|
      ind = @headers.find_index(cond.header)
      if ind == nil
        return false
      end
      cell = CSV.parse_line(line).map(&:to_s)[ind]
      cond.test(cell)
    }
  end

  def filter(new_file, *conds)
    File.open(new_file, "w") { |nf|
      File.open(@file) { |f|
        nf.write(f.readline)
        until f.eof?
          l = f.readline
          if line_check(l, conds) then nf.write(l) end
        end
      }
    }
  end
end
