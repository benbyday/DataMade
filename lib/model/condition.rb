class Condition
  attr_reader :header
  
  def initialize(header, &block)
    @header = header
    @test = block
  end

  def test(check)
    @test.call(check)
  end
end
