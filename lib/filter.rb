require 'rubygems'
require 'bundler/setup'
require 'date'
require 'pry-byebug'
require_relative 'model/spreadsheet'
require_relative 'model/condition'

file = if ARGV.length > 0 then ARGV[0] else "rec/legislators.csv" end
dest = if ARGV.length > 1 then ARGV[1] else "rec" end
sheet = Spreadsheet.new(file)

dem = Condition.new("party") { |check|
  check == "D"
}
u45 = Condition.new("birthdate") { |check|
    ((Date.today - Date.parse(check)).to_i/365.25).to_i < 45
}
rep = Condition.new("party") { |check|
  check == "R"
}
twit = Condition.new("twitter_id") { |check|
  !check.strip.empty?
}
yt = Condition.new("youtube_url") { |check|
  !check.strip.empty?
}

sheet.filter(dest+"/youngdem.csv", dem, u45)
sheet.filter(dest+"/socialrep.csv", rep, twit, yt)
